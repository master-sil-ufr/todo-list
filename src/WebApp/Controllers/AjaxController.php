<?php

namespace App\WebApp\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use App\Core\Services\ServiceInterface\CommentServiceInterface;
use App\Core\Services\ServiceInterface\TodoServiceInterface;
use App\Core\Services\ServiceInterface\UserServiceInterface;

use App\WebApp\RequestFactory\CommentRequestFactory;
use App\WebApp\RequestFactory\TodoRequestFactory;
use App\WebApp\RequestFactory\UserRequestFactory;

class AjaxController {
    private TodoServiceInterface $todoService;
    private CommentServiceInterface $commentService;
    private UserServiceInterface $userService;

    public function __construct(UserServiceInterface $userService, TodoServiceInterface $todoService, CommentServiceInterface $commentService)
    {
        $this->userService = $userService;
        $this->todoService = $todoService;
        $this->commentService = $commentService;
    }


    /**
     *
     */
    public function ajaxTest(Request $request, Response $response, array $args): Response
    {
        $response->getBody()->write(json_encode("Hello world"));
        return $response;
    }

    /**
     * Add a comment to database for a todo
     * returns list of comments encoded in json
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function addComment(Request $request, Response $response, array $args): Response
    {
        $commentRequest = CommentRequestFactory::fromServerRequest($request);

        $this->commentService->addComment($commentRequest->getComment(), $_SESSION['user']['id'], $commentRequest->getTask_id());
        return $this->getComments($request, $response, ['todo' => $commentRequest->getTask_id()]);
    }

    /**
     * Get list of users
     * returns list of users encoded in json
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function getUsers(Request $request, Response $response, array $args): Response
    {
        $users = $this->userService->getUsers();

        $response->getBody()->write(json_encode($users));
        return $response;
    }

    /**
     * Filter list of todos according to a user or keywords
     * returns list of todos encoded in json
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function filterTodos(Request $request, Response $response, array $args): Response
    {
        $todoRequest = TodoRequestFactory::fromServerRequest($request);
        $userRequest = UserRequestFactory::fromServerRequest($request);
                
        $todoObjects = $this->todoService->getTodos($userRequest->getEmail(), $todoRequest->getKeyword());

        $todo = array();

        for ($i=0; $i < count($todoObjects); $i++) { 
            
            $todo[] = [
                'id' => $todoObjects[$i]->getId(),
                'createdBy' => $todoObjects[$i]->getCreatedBy(),
                'assignedTo' => $todoObjects[$i]->getAssignedTo(),
                'title' => $todoObjects[$i]->getTitle(),
                'description' => $todoObjects[$i]->getDescription(),
                'createdAt' => $todoObjects[$i]->getCreatedAt(),
                'dueDate' => $todoObjects[$i]->getDueDate(),
            ];
        }
        
        $response->getBody()->write(json_encode($todo));
        return $response;
    }

    /**
     * Get a todo from its ID
     * returns a todo entity encoded in json
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function searchTodo(Request $request, Response $response, array $args): Response
    {
        $todoRequest = TodoRequestFactory::fromServerArgs($args);

        $todoObject = $this->todoService->getTodoById($todoRequest->getId());
        $createdByUser = $this->userService->getUserById($todoObject->getCreatedBy());
        $assignedToUser = ($todoObject->getAssignedTo() !== null) ? $this->userService->getUserById($todoObject->getAssignedTo()) : null;


        $todoInfos = array();
        $todoInfos[] = [
            'id' => $todoObject->getId(),
            'created_by' => $todoObject->getCreatedBy(),
            'created_by_email' => $createdByUser->getEmail(),
            'assigned_to' => $todoObject->getAssignedTo(),
            'assigned_to_email' => ($assignedToUser !== null) ? $assignedToUser->getEmail() : null,
            'title' => $todoObject->getTitle(),
            'description' => $todoObject->getDescription(),
            'created_at' => $todoObject->getCreatedAt(),
            'due_date' => $todoObject->getDueDate(),
        ];


        $response->getBody()->write(json_encode($todoInfos));
        return $response;
    }

    /**
     * Get list of comments for a todo
     * returns list of comments encoded in json
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function getComments(Request $request, Response $response, array $args): Response
    {  
        $commentRequest = CommentRequestFactory::fromServerArgs($args);

        $comments = $this->commentService->getComments($commentRequest->getTodoId());

        $commentsInfos = array();
        for ($i=0; $i < count($comments); $i++) { 
            
            $commentsInfos[] = [
                'id' => $comments[$i]->getId(),
                'task_id' => $comments[$i]->getTask_id(),
                'created_by' => $comments[$i]->getCreated_by(),
                'created_at' => $comments[$i]->getCreated_at(),
                'comment' => $comments[$i]->getComment()
            ];
        }

        $response->getBody()->write(json_encode($commentsInfos));
        return $response;  

    }
}
