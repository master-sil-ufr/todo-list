<?php
namespace App\WebApp\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Views\Twig;

/**
 * Helps to render views and display messages to the front
 */
class Controller{
    /**
     * Render the page to the app from the given file with arguments
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @param $file
     * @return Response
     *
     * @throws \Exception
     */
    public function render(Request $request, Response $response, array $args,  $file): Response
    {
        $view = Twig::fromRequest($request);
        try {
            return $view->render($response, $file, $args);
        } catch (\Exception $e) {
            throw new \Exception('File not found');
        }
    }

    /**
     * Display message to user
     * @param array $message
     * @return-void
     */
    public function setFlash(array  $message)
    {
        $_SESSION['flash'] = $message;
    }
}