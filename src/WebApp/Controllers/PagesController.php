<?php
namespace App\WebApp\Controllers;

use Exception;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use App\Core\Entities\Session;
use App\Core\Services\ServiceInterface\CommentServiceInterface;
use App\Core\Services\ServiceInterface\TodoServiceInterface;
use App\Core\Services\ServiceInterface\UserServiceInterface;
use App\WebApp\RequestFactory\CommentRequestFactory;
use App\WebApp\RequestFactory\TodoRequestFactory;
use App\WebApp\RequestFactory\UserRequestFactory;

class PagesController extends Controller{
    private TodoServiceInterface $todoService;
    private CommentServiceInterface $commentService;
    private UserServiceInterface $userService;

    public function __construct(UserServiceInterface $userService, TodoServiceInterface $todoService, CommentServiceInterface $commentService)
    {
        $this->userService = $userService;
        $this->todoService = $todoService;
        $this->commentService = $commentService;
    }

    /**
     * Render home view
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     *
     * @throws Exception
     */
    public function home(Request $request, Response $response, array $args): Response
    {
        if (isset($_SESSION['user'])) {
            return $response->withStatus(302)->withHeader('Location', '/dashboard'); 
        }
        return $this->render($request, $response, $args, 'templates/home.html.twig');
        
    }

    /**
     * Render login view
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     *
     * @throws Exception
     */
    public function login(Request $request, Response $response, array $args): Response
    {
        if (isset($_SESSION['user'])) {
            return $response->withStatus(302)->withHeader('Location', '/dashboard'); 
        }
        $flash = $_SESSION['flash'] ?? [];
        $_SESSION['flash'] = [];
        $args['flash'] = $flash;

        return $this->render($request, $response, ['flash' => $args['flash']], 'templates/login.html.twig');
    }

    /**
     * Render dashboard view
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     *
     * @throws Exception
     */
    public function dashboard(Request $request, Response $response, array $args): Response
    {
        if (!isset($_SESSION['user']) || $_SESSION['user'] == null) {
            Session::stopSession();
            return $response->withStatus(302)->withHeader('Location', '/');
        }

        $userRequest = UserRequestFactory::fromServerRequest($request);
        $userObject = $this->userService->getUsers($userRequest);

        
        $todoRequest = TodoRequestFactory::fromServerRequest($request);
        $todoObjects = $this->todoService->getTodos('null', '');
        $todoInfos = array();
        $commentsInfos = array();
        if ($todoObjects != null) {

            $todoByIdObject = $this->todoService->getTodoById($todoObjects[0]->getId());
            $createdByUser = $this->userService->getUserById($todoByIdObject->getCreatedBy());
            $assignedToUser = ($todoByIdObject->getAssignedTo() !== null) ? $this->userService->getUserById($todoByIdObject->getAssignedTo()) : null;
            $commentRequest = CommentRequestFactory::fromServerArgs(['todo' => $todoByIdObject->getId()]);
            $comments = $this->commentService->getComments($commentRequest->getTodoId());

            for ($i=0; $i < count($comments); $i++) {
                
                $commentsInfos[] = [
                    'id' => $comments[$i]->getId(),
                    'task_id' => $comments[$i]->getTask_id(),
                    'created_by' => $comments[$i]->getCreated_by(),
                    'created_at' => $comments[$i]->getCreated_at(),
                    'comment' => $comments[$i]->getComment()
                ];
            }
            
        $todoInfos[] = [
            'id' => $todoByIdObject->getId(),
            'created_by' => $todoByIdObject->getCreatedBy(),
            'created_by_email' => $createdByUser->getEmail(),
            'assigned_to' => $todoByIdObject->getAssignedTo(),
            'assigned_to_email' => ($assignedToUser !== null) ? $assignedToUser->getEmail() : null,
            'title' => $todoByIdObject->getTitle(),
            'description' => $todoByIdObject->getDescription(),
            'created_at' => $todoByIdObject->getCreatedAt(),
            'due_date' => $todoByIdObject->getDueDate(),
        ];
    }

        $flash = $_SESSION['flash'] ?? [];
        $_SESSION['flash'] = [];

        
        
        $args['todos'] = $todoObjects;
        $args['todo'] = $todoInfos[0];
        $args['user'] = $_SESSION['user'];
        $args['users'] = $userObject;
        $args['comments'] = $commentsInfos;
        $args['flash'] = $flash;


        return $this->render($request, $response, ['comments' => $args['comments'], 'todos' => $args['todos'], 'todo' => $args['todo'], 'user' => $args['user'], 'users' => $args['users'], 'flash' => $args['flash']], 'templates/dashboard.html.twig');
    }

    /**
     * Render signup view
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     *
     * @throws Exception
     */
    public function signup(Request $request, Response $response, array $args): Response
    {
        $flash = $_SESSION['flash'] ?? [];
        $_SESSION['flash'] = [];

        $args['flash'] = $flash;
        return $this->render($request, $response, ['flash' => $args['flash']], 'templates/signup.html.twig');
    }

    /**
     * Log-out user and render home view
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     *
     * @throws Exception
     */
    public function logout(Request $request, Response $response, array $args): Response
    {
        Session::stopSession();
        return $response->withStatus(302)->withHeader('Location', '/');
    }

}