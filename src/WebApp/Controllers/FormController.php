<?php
namespace App\WebApp\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use App\Core\Services\ServiceInterface\CommentServiceInterface;
use App\Core\Services\ServiceInterface\TodoServiceInterface;
use App\Core\Services\ServiceInterface\UserServiceInterface;

use App\WebApp\RequestFactory\TodoRequestFactory;
use App\WebApp\RequestFactory\UserRequestFactory;

use App\Core\Entities\Session;

class FormController extends Controller {
    private TodoServiceInterface $todoService;
    private CommentServiceInterface $commentService;
    private UserServiceInterface $userService;

    public function __construct(UserServiceInterface $userService, TodoServiceInterface $todoService, CommentServiceInterface $commentService)
    {
        $this->userService = $userService;
        $this->todoService = $todoService;
        $this->commentService = $commentService;
    }

    /**
     * Check if the user exist in database and render user
     * Create session if user exist
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function getConnexion(Request $request, Response $response, array $args): Response
    {
        $userRequest = UserRequestFactory::fromServerRequest($request);
        $user = $this->userService->checkConnexion($userRequest);

        if (!$user || !password_verify($userRequest->getPwd(), $user->getPwd())) {
            $this->setFlash(['error' => 'Aucun utilisateur avec ces identifiants']);
            return $response->withStatus(302)->withHeader('Location', '/login');
        }

        Session::startSession();

        $_SESSION['user'] = [
            'id' => $user->getId(),
            'email' => $user->getEmail(),
            'password' => $user->getPwd()
        ];

        return $response->withStatus(302)->withHeader('Location', '/dashboard');
    }

    /**
     * Create an account and log-in the new user
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function signUpUser(Request $request, Response $response, array $args): Response
    {
        $userRequest = UserRequestFactory::fromServerRequest($request);

        $email = $userRequest->getEmail();
        $pwd = $userRequest->getPwd();
        $confPWD = $userRequest->getConfirmPWD();

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->setFlash(['error' => 'Email non conforme']);
            return $response->withStatus(302)->withHeader('Location', '/signup');
          }

        if($pwd != $confPWD) {
            $this->setFlash(['error' => 'Les mots de passe ne sont pas identiques']);
            return $response->withStatus(302)->withHeader('Location', '/signup');
        }

        // Check if the user already exist
        $getExistingUser = $this->userService->checkConnexion($userRequest);

        if ($getExistingUser) {
            $this->setFlash(['error' => 'Utilisateur déjà existant']);
            return $response->withStatus(302)->withHeader('Location', '/signup');
        }

        $pwd = password_hash($pwd, PASSWORD_DEFAULT);
        $userRequest->setPwd($pwd);

        $user = $this->userService->createUser($userRequest);

        Session::startSession();

        $_SESSION['user'] = [
            'id' => $user->getId(),
            'email' => $user->getEmail(),
            'password' => $user->getPwd()
        ];


        return $response->withStatus(302)->withHeader('Location', '/dashboard');
    }

    /**
     * Inserts a new todo for the user
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function todoInsertion(Request $request, Response $response, array $args): Response
    {
        $todoRequest = TodoRequestFactory::fromServerRequest($request);

        if ($todoRequest->getTitle() === '') {
            $this->setFlash(['error' => 'Titre non valide']);
            return $response->withStatus(302)->withHeader('Location', '/dashboard');

        }
        $title = $todoRequest->getTitle();
        if ($todoRequest->getDescription() === '') {
            $this->setFlash(['error' => 'Description manquante']);

            return $response->withStatus(302)->withHeader('Location', '/dashboard');
        }
        $description = $todoRequest->getDescription();
        $assignedTo = $todoRequest->getAssignedTo();
        $dueDate =  $todoRequest->getDueDate();
        if (!isset($_SESSION['user']['id'])) {
            $this->setFlash(['error' => 'Utilisateur non connecté']);
            return $response->withStatus(302)->withHeader('Location', '/login');
        }
        $id = $_SESSION['user']['id'];

        if ($assignedTo !== 'null') {
            $user = $this->userService->getUserByEmail($assignedTo);
            $todoRequest->setAssignedToId($user->getId());
        }else {
            $todoRequest->setAssignedToId(null);
        }
        $todoRequest->setCreatedBy($id);

        $query = $this->todoService->insertTodo($todoRequest);

        $this->setFlash(['success' => 'La todo a bien été insérée']);
        
        return $response->withStatus(302)->withHeader('Location', '/dashboard');
    }

    /**
     * Delete a todo
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function deleteTodo(Request $request, Response $response, array $args): Response
    {
        $todoRequest = TodoRequestFactory::fromServerArgs($args);
        $this->todoService->deleteTodo($todoRequest->getId());

        $this->setFlash(['success' => 'La todo a bien été supprimée']);

        // Add arguments of success for delete
        return $response->withStatus(302)->withHeader('Location', '/dashboard');
    }
}