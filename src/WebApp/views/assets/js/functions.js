const Functions = {
    setusers : function (users) {
    users = $.parseJSON(users)
    console.log(users, users.length, typeof users)
    let options = '';
    for (let i = 0; i < users.length; i++) {
        options += '<option value="' + i + '">' + users[i]['email'] + '</option>\n'
    }
    $('.task-kind').empty();
    $('.task-kind').append('<select id="userTask" class="form-select">\
            ' + options +    '\
            </select>');
    },

    setfilter : function (filter) {
    console.log(filter)
    filter = $.parseJSON(filter)
    $('.liste').empty()
    filter.forEach(f => {
        $('.liste').append("<a class='todoLink' id='todoLink"+f['id'] +"' href=\"javascript:void(0);\">" + f['title'] + ' </a>')
    });
    },

    settodocontainer : function (todo) {
        todo = $.parseJSON(todo)
        todo = todo[0]
    $(".MainContent .TodoContent h2").text(todo['title'])
    if(todo['assigned_to']==null && todo['due_date']!=null)
        $(".MainContent .TodoContent .assigned").text("A faire avant le "+todo['due_date'])
    else if(todo['assigned_to']!=null && todo['due_date']==null)
        $(".MainContent .TodoContent .assigned").text("A faire par "+todo['assigned_to_email'])
    else if(todo['assigned_to']!=null && todo['due_date']!=null)
        $(".MainContent .TodoContent .assigned").text("A faire par "+todo['assigned_to_email']+" avant le "+todo['due_date'])
    else
        $(".MainContent .TodoContent .assigned").text("")
    $(".MainContent .TodoContent textarea").text(todo['description'])
    $(".MainContent .TodoContent .creator").text("Créé par " + todo['created_by_email'] + " le " + todo['created_at'])
    $(".MainContent .TodoContent #deleteTodo").attr("action", "/dashboard/delete/todo/"+todo['id'])
    $("#taskId").val(todo['id'])

    },

    setcommentcontainer : function (comment) {
        console.log(comment)
        comment = $.parseJSON(comment)
        $('.displayComments').empty()
        console.log(comment.length)
        if (comment.length != 0) {

            let list = ''
            for (let i = comment.length-1; i >= 0; i--) {
                // Ajouter la date du commentaire
                list += '<li> [' + comment[i]['created_at'] + ' ] ' + comment[i]['comment'] + '</li>'
            }
            
            $('.displayComments').append('<ul>' + list + '</ul>')
        }
    },
}

export default Functions;