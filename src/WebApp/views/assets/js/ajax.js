const Ajax = {
    filter : async function (data, callback) {  
        await $.post("/dashboard", data, callback)
    },

    getTodo : async function (link, callback) {
        await $.get(link, callback)
    },

    getComments : async function (link, callback) {
        await $.get(link, callback)
    },
    
    getUsers :async function (callback) {
        await $.get("/users", callback)
    },

    addComment: async function (data, callback) {
        await $.post("/dashboard/add/comment", data, callback)
    }
};


export default Ajax