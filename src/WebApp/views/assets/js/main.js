import  Ajax  from "./ajax";
import Functions from "./functions";

$('#todo').click(function(event) {
    event.preventDefault();

    $.get("/dashboard/ajax", function (response) {  
        $("#res").load("/dashboard/ajax")
    })
})


$('.form-select').change( async function () { 
    console.log($(this).val())
    if ($(this).val() == 1) {
        Ajax.getUsers(setusers)
    }else if ($(this).val() == 2) {
        $('.task-kind').empty();
        $('.task-kind').append('<input id="Search" type="text" name="Search" placeholder="Mot clé" class="form-control"/>');
    }
});

$('#filter').submit(function (event) {
    let form = $('#filter').serializeArray()
    event.preventDefault()
    Ajax.filter(form, Functions.setfilter)
})

$('.liste').on('click', '.todoLink', function (e) {
    e.preventDefault()
    let todoLinkId = $(this).attr('id');
    let todoId = todoLinkId.substring("todoLink".length,todoLinkId.length)
    let todolink = "/dashboard/todo/" + todoId
    let commentlink = "/dashboard/comment/" + todoId

    console.log('todoLink' + todolink);
    console.log('todoLinkId' + todoLinkId);
    console.log('todoId' + todoId);

    Ajax.getTodo(todolink, Functions.settodocontainer)
    Ajax.getComments(commentlink, Functions.setcommentcontainer)
})

$('.AddComment').submit(function (e) {
    let form = $('.AddComment').serializeArray()
    e.preventDefault()
    
    $('.AddComment textarea').val("");

    Ajax.addComment(form, Functions.setcommentcontainer)
})

$('.flash').show()
console.log('hello ' + $('.flash'))
setTimeout(() => $('.flash').hide())