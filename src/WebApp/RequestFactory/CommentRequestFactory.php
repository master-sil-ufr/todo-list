<?php

namespace App\WebApp\RequestFactory;

use App\Core\Requests\CommentRequest;
use Psr\Http\Message\ServerRequestInterface;

class CommentRequestFactory
{
    /**
     * Parse data from request and return a Comment Request
     * @param ServerRequestInterface $request
     *
     * @return CommentRequest
     */
    public static function fromServerRequest(ServerRequestInterface $request): CommentRequest
    {
        $params = $request->getParsedBody();
        return new CommentRequest(
            (int) htmlspecialchars($params['taskId']),
            (isset($params['createdBy']))? (int) htmlspecialchars($params['createdBy']) : 0,
            (isset($params['createdAt'])) ? htmlspecialchars($params['createdAt']) : '',
            (isset($params['comment']))? htmlspecialchars($params['comment']) : '',
            ($params['todoId'] !== null) ? (int) htmlspecialchars($params['todoId']) : 0
        );
    }

    /**
     * Parse data from args and return a Comment Request
     *
     * @param array $args
     *
     * @return CommentRequest
     */
    public static function fromServerArgs(array $args): CommentRequest
    {

        $todo = $args['todo'];
        return new CommentRequest(
            (isset($args['task_id'])) ? (int) $args['task_id'] : 0,
            (isset($args['created_by'])) ? (int) $args['created_by'] : 0,
            (isset($args['created_at'])) ? $args['created_at'] : '',
            (isset($args['comment'])) ? $args['comment'] : '',
            (int) htmlspecialchars($todo)
        );
    }
}
