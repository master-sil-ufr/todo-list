<?php

namespace App\WebApp\RequestFactory;

use Psr\Http\Message\ServerRequestInterface;
use App\Core\Requests\UserRequest;

class UserRequestFactory
{
    /**
     * Parse data from request and return a Comment Request
     * @param ServerRequestInterface $request
     *
     * @return UserRequest
     */
    public static function fromServerRequest(ServerRequestInterface $request): UserRequest
    {
        $params = $request->getParsedBody();
        return new UserRequest(
            htmlspecialchars($params['email']),
            htmlspecialchars($params['password']),
            isset($params['Confirmpassword']) ? htmlspecialchars($params['Confirmpassword']) : ''
        );
    }
}
