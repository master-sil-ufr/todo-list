<?php

namespace App\WebApp\RequestFactory;

use Psr\Http\Message\ServerRequestInterface;
use App\Core\Requests\TodoRequest;

class TodoRequestFactory
{
    /**
     * Parse data from request and return a Comment Request
     * @param ServerRequestInterface $request
     *
     * @return TodoRequest
     */
    public static function fromServerRequest(ServerRequestInterface $request): TodoRequest
    {
        $params = $request->getParsedBody();
        return new TodoRequest(
            isset($params['id']) ? (int) htmlspecialchars($params['id']) : 0,
            isset($params['created_by']) ? (int) htmlspecialchars($params['created_by']) : 0,
            htmlspecialchars($params['AssignedTo']),
            isset($params['title']) ? htmlspecialchars($params['title']) : '',
            isset($params['description']) ? htmlspecialchars($params['description']) : '',
            htmlspecialchars($params['dueDate']),
            htmlspecialchars($params['keyword'])
        );
    }

    /**
     * Parse data from args and return a Comment Request
     *
     * @param array $args
     *
     * @return TodoRequest
     */
    public static function fromServerArgs(array $args): TodoRequest
    {
        $todo = $args['todo'];
        return new TodoRequest(
            (int) $todo ?? 0,
            isset($args['created_by']) ? htmlspecialchars($args['created_by']) : 0,
            htmlspecialchars($args['AssignedTo']),
            isset($args['title']) ? htmlspecialchars($args['title']) : '',
            isset($args['description']) ? htmlspecialchars($args['description']) : '',
            htmlspecialchars($args['dueDate']),
            htmlspecialchars($args['keyword'])
        );
    }
}
