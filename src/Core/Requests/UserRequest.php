<?php

namespace App\Core\Requests;

class UserRequest {

    private $email;
    private $pwd;
    private $confirmPWD;

    public function __construct( $email , $pwd, $confirmPWD = '' )
    {
        $this->email=$email;
        $this->pwd=$pwd;
        $this->confirmPWD = $confirmPWD;
    }

    public function getPwd(): ?string
    {
        return $this->pwd;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getConfirmPWD() : ?string
    {
        return $this->confirmPWD;
    }

    public function setPwd(string $pwd)
    {
        $this->pwd = $pwd;
    }
}