<?php

namespace App\Core\Requests;

class CommentRequest {

    private int $task_id;
    private int $created_by;
    private string $created_at;
    private string $comment;
    private int $todo_id;

    public function __construct( int $task_id, int $created_by, string $created_at, string $comment, int $todo_id)
    {
        $this->task_id=$task_id;
        $this->created_by=$created_by;
        $this->created_at=$created_at;
        $this->comment=$comment;
        $this->todo_id=$todo_id;
    }

    public function getTask_id()
    {
        return $this->task_id;
    }

    public function getCreated_by()
    {
        return $this->created_by;
    }

    public function getCreated_at()
    {
        return $this->created_at;
    }
    
    public function getComment()
    {
        return $this->comment;
    }

    public function getTodoId()
    {
        return $this->todo_id;
    }

}