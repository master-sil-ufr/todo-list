<?php

namespace App\Core\Requests;

class TodoRequest
{
    private int $id;
    private int $createdBy;
    private ?string $assignedTo;
    private ?int $assignedToId = null;
    private string $title;
    private string $description;
    private ?string $dueDate;
    private ?string $keyword;

    public function __construct(int $id, int $createdBy, ?string $assignedTo, string $title, string $description,?string $dueDate, ?string $keyword)
    {
        $this->id = $id;
        $this->createdBy = $createdBy;
        $this->assignedTo = $assignedTo;
        $this->title = $title;
        $this->dueDate= $dueDate;
        $this->description= $description;
        $this->keyword = $keyword;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreatedBy(): int
    {
        return $this->createdBy;
    }

    public function getAssignedTo(): ?string
    {
        return $this->assignedTo;
    }

    public function getAssignedToId(): ?int
    {
        return $this->assignedToId;
    }

    public function getTitle(): string
    {
        return $this->title;
    }
    
    public function getDescription(): string
    {
        return $this->description;
    }
    
    public function getDueDate(): ?string
    {
        return $this->dueDate;
    }

    public function getKeyword() : ?string
    {
        return $this->keyword;
    }

    public function setAssignedToId($id) 
    {
        $this->assignedToId = $id;
    }

    public function setCreatedBy($id) 
    {
        $this->createdBy = $id;
    }

}
