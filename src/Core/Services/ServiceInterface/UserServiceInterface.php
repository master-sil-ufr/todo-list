<?php
namespace  App\Core\Services\ServiceInterface;

use App\Core\Requests\UserRequest;
use App\Core\Entities\User;

interface UserServiceInterface
{
    public function checkConnexion(UserRequest $userRequest);
    public function createUser(UserRequest $userRequest);
    public function getUsers():array;
    public function getUserById(int $id):User;
    public function getUserByEmail($email) : User;
}