<?php
namespace  App\Core\Services\ServiceInterface;

interface CommentServiceInterface
{
    public function addComment(string $comment, $userID, $taskId);
    public function getComments(int $taskId): array; 
}