<?php
namespace  App\Core\Services\ServiceInterface;

use App\Core\Requests\TodoRequest;
use App\Core\Entities\Todo;

interface TodoServiceInterface
{
    public function insertTodo(TodoRequest $todoRequest);
    public function getTodos($email, $keyword) : array;
    public function getTodoById($id) : Todo;
    public function deleteTodo($id);   
}