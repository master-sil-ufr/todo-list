<?php
namespace  App\Core\Services;

use App\Core\Entities\Todo;
use App\Core\Exceptions\TodoExceptions\BadTodoIdException;
use App\Core\Services\ServiceInterface\TodoServiceInterface;
use App\Core\RepositoryInterface\TodoRepositoryInterface;
use App\Core\Requests\TodoRequest;

class TodoService implements TodoServiceInterface
{
    private TodoRepositoryInterface $todoRepository;

    public function __construct(TodoRepositoryInterface $todoRepository)
    {
        $this->todoRepository = $todoRepository;
    }

    public function insertTodo(TodoRequest $todoRequest)
    {
        return $this->todoRepository->insertTodo($todoRequest);

    }

    public function getTodos($email, $keyword) : array
    {
        return $this->todoRepository->getTodos($email, $keyword);
    }

    /**
     * @throws BadTodoIdException
     */
    public function getTodoById($id) : Todo
    {
        if ($id <= 0) {
            throw new BadTodoIdException('Id de la todo non prise en charge');
        }
        return $this->todoRepository->getTodoById($id) ;
    }
    
    /**
     * Delete a user
     *
     * @param int id
     */
    public function deleteTodo($id)
    {
        $this->todoRepository->deleteTodo($id) ;
    } 
}