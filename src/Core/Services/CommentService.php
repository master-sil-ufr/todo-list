<?php

namespace App\Core\Services;

use App\Core\Exceptions\CommentExceptions\EmptyCommentException;
use App\Core\Services\ServiceInterface\CommentServiceInterface;
use App\Core\RepositoryInterface\CommentRepositoryInterface;

class CommentService implements CommentServiceInterface
{
    private CommentRepositoryInterface $commentRepository;

    public function __construct(CommentRepositoryInterface $commentRepository)
    {
        $this->commentRepository = $commentRepository;
    }

    /**
     * @throws EmptyCommentException
     */
    public function addComment(string $comment, $userID, $taskId)
    {
        if ($comment === '') {
            throw new EmptyCommentException('Le commentaire ne peut être vide');
        }
        return $this->commentRepository->addComment($comment, $userID, $taskId);
    }

    public function getComments(int $taskId): array
    {
        return $this->commentRepository->getComments($taskId);
    }
}