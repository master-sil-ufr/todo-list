<?php
namespace  App\Core\Services;

use App\Core\Entities\User;
use App\Core\Exceptions\UserExceptions\NullUserException;
use App\Core\Services\ServiceInterface\UserServiceInterface;
use App\Core\RepositoryInterface\UserRepositoryInterface;
use App\Core\Requests\UserRequest;
use Exception;

class UserService implements UserServiceInterface
{
    private UserRepositoryInterface $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Return connected user
     *
     * @param UserRequest $userRequest
     * @return false
     */
    public function checkConnexion(UserRequest $userRequest)
    {
        try {
            return $this->userRepository->checkConnexion($userRequest);
        }catch (Exception $e) {
           return false;
        }
    }
    

    /**
     * Create a user
     *
     * @param UserRequest $userRequest
     * @return User user
     */
    public function createUser(UserRequest $userRequest): User
    {
        return $this->userRepository->createUser($userRequest);
    }

    /**
     * Get all users
     *
     * @return array users
     */
    public function getUsers():array
    {
        return $this->userRepository->getUsers();
    }
    
    /**
     * Get a user with an id
     *
     * @param int $id
     * @return User user
     */
    public function getUserById(int $id):User
    {
        return $this->userRepository->getUserById($id);
    }

    /**
     * Get a user with an email
     *
     * @param string $email
     * @throws NullUserException
     * @return User user
     */
    public function getUserByEmail($email) : User
    {
        if ($email === 'null') {
            throw new NullUserException('Utilisateur ne peut être nul');
        }
        return $this->userRepository->getUserByEmail($email);
    }
}