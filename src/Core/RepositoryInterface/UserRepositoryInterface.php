<?php

namespace App\Core\RepositoryInterface;

use App\Core\Requests\UserRequest;
use App\Core\Entities\User;

interface UserRepositoryInterface
{
    public function checkConnexion(UserRequest $userRequest);
    public function createUser(UserRequest $userRequest) : User;
    public function getUsers():array;
    public function getUserById(int $id): User;
    public function getUserByEmail($email) : User;
}