<?php

namespace App\Core\RepositoryInterface;

use App\Core\Requests\TodoRequest;
use App\Core\Entities\Todo;

interface TodoRepositoryInterface
{
    public function insertTodo(TodoRequest $todoRequest);
    public function getTodos($email, $keyword): array ;
    public function getTodoById( $id): Todo ;
    public function deleteTodo( $id);
}