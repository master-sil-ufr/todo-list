<?php

namespace App\Core\RepositoryInterface;

interface CommentRepositoryInterface
{    
    public function addComment(string $comment, $userID, $taskId);
    public function getComments(int $taskId): array;
}