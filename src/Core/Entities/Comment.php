<?php

namespace App\Core\Entities;

/**
   * Comment objects
   */
class Comment {

    private $id;
    private $task_id;
    private $created_by;
    private $created_at;
    private $comment;

    public function __construct($id,  $task_id, $created_by, $created_at, $comment)
    {
        $this->task_id=$task_id;
        $this->created_by=$created_by;
        $this->created_at=$created_at;
        $this->comment=$comment;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTask_id()
    {
        return $this->task_id;
    }

    public function getCreated_by()
    {
        return $this->created_by;
    }

    public function getCreated_at()
    {
        return $this->created_at;
    }
    
    public function getComment()
    {
        return $this->comment;
    }

}