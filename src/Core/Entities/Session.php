<?php

namespace App\Core\Entities;

/**
     * Session objects
     */
class Session {

    public function isConnected(): bool
    {
        return array_key_exists('user', $_SESSION) && $_SESSION['user']['id'] != null;
    }

    public static function startSession()
    {
        if (session_status() === PHP_SESSION_NONE)
            session_start();

    }

    public static function stopSession()
    {
        session_unset();
        session_destroy();
    }
}