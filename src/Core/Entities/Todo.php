<?php

namespace App\Core\Entities;
use JsonSerializable;

/**
     * Todo objects 
     * implements JsonSerializable
     */
class Todo implements JsonSerializable {

    private $id;
    private $createdBy;
    private $assignedTo;
    private $title;
    private $description;
    private $createdAt;
    private $dueDate;

    public function __construct($id, $createdBy, $assignedTo, $title, $description, $createdAt, $dueDate)
    {
        $this->id=$id;
        $this->createdBy = $createdBy;
        $this->assignedTo=$assignedTo;
        $this->title=$title;
        $this->description=$description;
        $this->createdAt = $createdAt;
        $this->dueDate=$dueDate;
    }
  
    public function getId()
    {
        return $this->id;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function getAssignedTo()
    {
        return $this->assignedTo;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    
    public function getDueDate()
    {
        return $this->dueDate;
    }

     public function jsonSerialize() 
     {
        return [
            'id' => $this->id,
            'createdBy' => $this->createdBy,
            'ass' =>$this->assignedTo,
            'title' =>$this->title,
            'description' =>$this->description,
            'createdAt' =>$this->createdAt,
            'dueDate' =>$this->dueDate
        ];
    }
}