<?php 

namespace App\Core\Entities;

/**
     * User objects
     */
class User {

    private $id;
    private $email;
    private $pwd;

    public function __construct($id, $email, $pwd)
    {
        $this->id=$id;
        $this->email=$email;
        $this->pwd=$pwd;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getPwd()
    {
        return $this->pwd;
    }

    public function getEmail()
    {
        return $this->email;
    }
}