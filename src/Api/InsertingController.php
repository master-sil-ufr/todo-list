<?php

namespace App\Api;
use App\Core\Exceptions\UserExceptions\UnknownUserException;
use App\Core\RepositoryInterface\UserRepositoryInterface;
use App\WebApp\RequestFactory\TodoRequestFactory;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use App\Core\RepositoryInterface\TodoRepositoryInterface;

class InsertingController {
    private TodoRepositoryInterface $todoRepository;
    private UserRepositoryInterface $userRepository;
    public function __construct(TodoRepositoryInterface $todoRepository, UserRepositoryInterface $userRepository)
    {
        $this->todoRepository = $todoRepository;
        $this->userRepository = $userRepository;
    }
    
    /**
     * Send API output.
     *
     * @param mixed  $data
     * @param string $httpHeader
     */
    protected function sendOutput($data, $httpHeaders=array())
    {
        header_remove('Set-Cookie');
 
        if (is_array($httpHeaders) && count($httpHeaders)) {
            foreach ($httpHeaders as $httpHeader) {
                header($httpHeader);
            }
        }
 
        echo $data;
        exit;
    }
    
    /**
     * Insert ToDo with API.
     *
     * @param Request  $request
     * @param Response $response
     */
    public function insert(Request $request, Response $response)
    {
        try {
            $callingUserEmail = $request->getHeader('Authorization')[0];
            $email= $this->userRepository->getUserByEmail($callingUserEmail);

            $todoRequest = TodoRequestFactory::fromServerRequest($request);
            $todos = $this->todoRepository->insertTodo($todoRequest);

            $responseData = json_encode(["succes" => "insertion réussie"]);
            $this->sendOutput(
                $responseData,
                array('Content-Type: application/json', 'HTTP/1.1 200 OK')
            );
            return $response;

        } catch (UnknownUserException $e) {
            $response->getBody()->write(json_encode(["error" => "unauthorized user"]));
            return $response->withStatus(401);
        }
    }
}
