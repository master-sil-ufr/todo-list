<?php

namespace App\Api;

use App\Core\Exceptions\UserExceptions\UnknownUserException;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use App\Core\RepositoryInterface\UserRepositoryInterface;
use App\Core\RepositoryInterface\TodoRepositoryInterface;

class ListingController {
    private TodoRepositoryInterface $todoRepository;
    private UserRepositoryInterface $userRepository;

    public function __construct(TodoRepositoryInterface $todoRepository, UserRepositoryInterface $userRepository)
    {
        $this->todoRepository = $todoRepository;
        $this->userRepository = $userRepository;
    }
    
    /**
     * Send API output.
     *
     * @param mixed  $data
     * @param string $httpHeader
     */
    protected function sendOutput($data, $httpHeaders=array())
    {
        header_remove('Set-Cookie');
 
        if (is_array($httpHeaders) && count($httpHeaders)) {
            foreach ($httpHeaders as $httpHeader) {
                header($httpHeader);
            }
        }
 
        echo $data;
        exit;
    }
    
    /**
     * Get list of todos
     *
     * @param Request  $request
     * @param Response $response
     * @return Response $response
     */
    public function list(Request $request, Response $response): Response
    {
        try {
            $callingUserEmail = $request->getHeader('Authorization')[0];
            $email= $this->userRepository->getUserByEmail($callingUserEmail);

            $listingRequest = GetListingRequestFactory::fromServerRequest($request);
            $todos =$this->todoRepository->getTodos($listingRequest->getEmail(),$listingRequest->getKeywords());

            $responseData = json_encode($todos);
            $this->sendOutput(
                $responseData,
                array('Content-Type: application/json', 'HTTP/1.1 200 OK')
            );
            return $response;

        } catch (UnknownUserException $e) {
            $response->getBody()->write(json_encode(["error" => "unauthorized user"]));
            return $response->withStatus(401);
        }    
    }
}
