<?php


namespace App\Api;

class GetListingRequest {

    private ?string $email;
    private ?string $keywords;

    public function __construct(?string $email, ?string $keywords)
    {
        $this->email = $email;
        $this->keywords = $keywords;
    }


    /**
     * Return keywords
     *
     * @return string keywords
     */
    public function getKeywords(): ?string
    {
        return $this->keywords;
    }


    /**
     * Return email
     *
     * @return string email
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

}