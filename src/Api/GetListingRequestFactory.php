<?php

namespace App\Api;

use Psr\Http\Message\ServerRequestInterface;

class GetListingRequestFactory
{
    /**
     * Prepares a request for the list method
     *
     * @return GetListingRequest data
     */
    public static function fromServerRequest(ServerRequestInterface $request): GetListingRequest
    {
        $params = $request->getParsedBody();
        return new GetListingRequest(
            isset($params['email']) ? (string) htmlspecialchars($params['email']) : 'null',
            isset($params['keywords']) ? (string) htmlspecialchars($params['keywords']) : ''
        );
    }
}
