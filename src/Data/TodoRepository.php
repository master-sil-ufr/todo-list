<?php
namespace App\Data;

use App\Core\Entities\Todo;
use App\Core\Exceptions\TodoExceptions\UnknownTodoIdException;
use App\Core\Requests\TodoRequest;
use App\Core\RepositoryInterface\TodoRepositoryInterface;
use PDO;

class TodoRepository implements TodoRepositoryInterface
{
    private PDO $pdo;

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Inserts a Todo into the database
     *
     */
    public function insertTodo(TodoRequest $todoRequest)
    {
        $assignedTo_id= $todoRequest->getAssignedToId();
        $createdBy = $todoRequest->getCreatedBy();
        $title = $todoRequest->getTitle();
        $description = $todoRequest->getDescription();
        $dueDate = $todoRequest->getDueDate();

        
        $request = "insert into todos (created_by, assigned_to, title, description, created_at, due_date) values (:created_by,:assigned_to,:title,:description,NOW(),:due_date);";
        $query = $this->pdo->prepare($request);
        $query->bindValue(':created_by', $createdBy, PDO::PARAM_INT);
        $query->bindValue(':assigned_to', $assignedTo_id, ($assignedTo_id == null) ? PDO::PARAM_NULL : PDO::PARAM_INT);
        $query->bindValue(':title', $title, PDO::PARAM_STR);
        $query->bindValue(':description', $description, PDO::PARAM_STR);
        $query->bindValue(':due_date', $dueDate, ($dueDate == null) ? PDO::PARAM_NULL : PDO::PARAM_STR);

        $query->execute();
    
    }

    /**
     * Returns an array of Todo objects
     *
     * @return array todos
     */
    public function getTodos($email = 'null', $keyword = '') : array
    {

        if (($email === 'null') && $keyword === '')
        {
            $request = 'select * from todos';
            $query = $this->pdo->prepare($request);
            $query->execute();
        }

        else if ($email === 'null')
        {
            $request = "select * from todos where title like concat('%',:keyword,'%') or description like concat('%',:keyword,'%');";
            $query = $this->pdo->prepare($request);
            $query->bindValue(':keyword', $keyword, PDO::PARAM_STR);
            $query->execute();
        }
        
        else if ($keyword === '')
        {
            $request = "select * from todos where created_by = (select id from users where email = :email) ;";
            $query = $this->pdo->prepare($request);
            $query->bindValue(':email', $email, PDO::PARAM_STR);
            $query->execute();
        }
        
        else 
        {
            $request = "select * from todos where created_by = (select id from users where email = :email) and (title like concat('%',:keyword,'%') or description like concat('%',:keyword,'%'));";
            $query = $this->pdo->prepare($request);
            $query->bindValue(':email', $email, PDO::PARAM_STR);
            $query->bindValue(':keyword', $keyword, PDO::PARAM_STR);
            $query->execute();
        }

        $result = $query->fetchAll(\PDO::FETCH_ASSOC);
        $array=[];
        if ($result) {
            for ($i = 0; $i < count($result); $i++)
            {
                $array[] = new Todo(
                    $result[$i]['id'],
                    $result[$i]['created_by'],
                    $result[$i]['assigned_to'],
                    $result[$i]['title'],
                    $result[$i]['description'],
                    $result[$i]['created_at'],
                    $result[$i]['due_date']
                );
            }
        }
        
        return $array;
    }


    /**
     * @throws UnknownTodoIdException
     */
    public function getTodoById($id) : Todo
    {
        $request = 'select * from todos where id = :id';
        $query = $this->pdo->prepare($request);
        $query->bindValue(':id', $id, PDO::PARAM_INT);
        $query->execute();
            
        $result = $query->fetch(\PDO::FETCH_ASSOC);

        if (!$result) {
            throw new UnknownTodoIdException('Aucune tache à cet id');
        }

        return  new Todo(
            $result['id'], 
            $result['created_by'],
            $result['assigned_to'],
            $result['title'],
            $result['description'],
            $result['created_at'],
            $result['due_date']);
    }

    public function deleteTodo($id)
    {
        $request = 'delete from todos where id = :id';
        $query = $this->pdo->prepare($request);
        $query->bindValue(':id', $id, PDO::PARAM_INT);
        $query->execute();
    }
}