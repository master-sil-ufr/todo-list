<?php
namespace App\Data;

use App\Core\RepositoryInterface\CommentRepositoryInterface;
use App\Core\Entities\Comment;
use PDO;

class CommentRepository implements CommentRepositoryInterface 
{
    private PDO $pdo;

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Add a comment into the database
     */
    public function addComment(string $comment, $userID, $taskId){
        $request = 'insert into comments (task_id, created_by, created_at, comment) values (:taskID, :userID, NOW(), :comment);';
        $query = $this->pdo->prepare($request);
        $query->bindValue(':taskID', $taskId, PDO::PARAM_INT);
        $query->bindValue(':userID', $userID, PDO::PARAM_INT);
        $query->bindValue(':comment', $comment, PDO::PARAM_STR);
        $query->execute();
    }
    
    /**
     * Returns an array of Comment objects
     *
     * @return array Comments
     */
    public function getComments(int $taskId) : array{
        $request = 'select * from comments where task_id = :taskId;';
        $query = $this->pdo->prepare($request);
        $query->bindValue(':taskId', $taskId, PDO::PARAM_INT);
        $query->execute();
        
        $result = $query->fetchAll(\PDO::FETCH_ASSOC);

        $array = array();
        for ($i = 0; $i < count($result); $i++)
        {
            $array[] = new Comment(
                $result[$i]['id'],
                $result[$i]['task_id'],
                $result[$i]['created_by'],
                $result[$i]['created_at'],
                $result[$i]['comment']
            );
        }

        return $array;
    }

}