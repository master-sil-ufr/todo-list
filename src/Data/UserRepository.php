<?php
namespace App\Data;

use App\Core\Exceptions\UserExceptions\UnknownUserException;
use App\Core\Requests\UserRequest;
use App\Core\RepositoryInterface\UserRepositoryInterface;
use App\Core\Entities\User;
use PDO;

class UserRepository implements UserRepositoryInterface
{
    private PDO $pdo;

    public function __construct(PDO $pdo){
        $this->pdo = $pdo;
    }

    /**
     * Checks if user exists in database
     *
     * @return User
     * @throws UnknownUserException
     */
    public function checkConnexion(UserRequest $userRequest): User
    {
        $request = "select * from users where email = :email";
        $query = $this->pdo->prepare($request);
        $query->bindValue(
            ':email', $userRequest->getEmail(), PDO::PARAM_STR
        );

        $query->execute();

        $response = $query->fetch();

        if (!$response) {
            throw new UnknownUserException("Utilisateur inconnu");
        }else {
            return new User($response['id'], $response['email'], $response['password']);
        }
    }

    /**
     * Create a user (insert into database)
     *
     * @return User after insertion (with ID)
     */
    public function createUser(UserRequest $userRequest) : User
    {
        $request = "insert into users (email, password) VALUES ( :email, :password)";
        $query = $this->pdo->prepare($request);
        $query->bindValue(':email', $userRequest->getEmail(), PDO::PARAM_STR);
        $query->bindValue(':password',$userRequest->getPwd(),PDO::PARAM_STR);
        $query->execute();

        $id = $this->pdo->lastInsertId();

        return new User($id, $userRequest->getEmail(), $userRequest->getPwd());


    }

    /**
     * get all the users
     *
     * @return array User
     */
    public function getUsers() : array{
        $request = 'select * from users';
        $query = $this->pdo->prepare($request);
        $query->execute();

        $result = $query->fetchAll(\PDO::FETCH_ASSOC);

        for ($i = 0; $i < count($result); $i++)
        {
            $array[] = new User(
                $result[$i]['id'], 
                $result[$i]['email'],
                $result[$i]['password']
            );
        }

        return $array;
    }

    /**
     * gets a user by ther id
     *
     * @return User
     * @throws UnknownUserException
     */
    public function getUserById($id) : User{
        $request = 'select * from users where id = :id';
        $query = $this->pdo->prepare($request);
        $query->bindValue(':id', $id, PDO::PARAM_INT);
        $query->execute();

        $result = $query->fetchAll(\PDO::FETCH_ASSOC);

        if (!$result) {
            throw new UnknownUserException("Utilisateur inconnu");
        }

        return new User(
            $result[0]['id'], 
            $result[0]['email'],
            $result[0]['password']
        );    
    }

    /**
     * gets a user by ther email
     *
     * @return User
     * @throws UnknownUserException
     */
    public function getUserByEmail($email) : User {
        $request = "select * from users where email = :email;";
        $query = $this->pdo->prepare($request);
        $query->bindValue(':email', $email, PDO::PARAM_STR);
        $query->execute();
        $result = $query->fetchAll(\PDO::FETCH_ASSOC);

        if (!$result) {
            throw new UnknownUserException("Aucun utilisateur avec cet identifiant : $email");
        }

        return new User(
            $result[0]['id'], 
            $result[0]['email'],
            $result[0]['password']
        );
    }
}