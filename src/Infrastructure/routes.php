<?php


use App\WebApp\Controllers\PagesController;
use App\WebApp\Controllers\FormController;
use App\WebApp\Controllers\AjaxController;
use App\Api\ListingController;
use App\Api\InsertingController;
use Slim\App;

// Api calls
$app->get('/api/todos', ListingController::class . ':list');
$app->post('/api/todos', InsertingController::class. ':insert');

$app->get('/', PagesController::class . ':home')->setName('home');
$app->get('/login', PagesController::class . ':login')->setName('login');
$app->get('/signup', PagesController::class . ':signup')->setName('signup');
$app->get('/dashboard', PagesController::class . ':dashboard')->setName('dashboard');
$app->get('/newTodo', PagesController::class . ':newTodo')->setName('newTodo');
$app->get('/logout', PagesController::class . ':logout')->setName('logout');

$app->get('/dashboard/todo/{todo}', AjaxController::class . ':searchTodo')->setName('todo');
$app->get('/dashboard/comment/{todo}', AjaxController::class . ':getComments')->setName('todo');
$app->get('/users', AjaxController::class . ':getUsers');

$app->post('/dashboard/add/comment', AjaxController::class . ':addComment')->setName('addComment');
// $app->post('/dashboard/add/task', FormController::class . ':addTask');
$app->post('/login', FormController::class . ':getConnexion');
$app->post('/signup', FormController::class . ':signUpUser');
$app->post('/newTodo', FormController::class . ':todoInsertion');
$app->post('/dashboard', AjaxController::class . ':filterTodos');

$app->post('/dashboard/delete/todo/{todo}', FormController::class . ':deleteTodo')->setName('deleteTodo');