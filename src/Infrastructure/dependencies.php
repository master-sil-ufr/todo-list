<?php
namespace App\Infrastructure;


use App\Core\Services\ServiceInterface\CommentServiceInterface;
use App\Core\Services\CommentService;
use App\Core\RepositoryInterface\CommentRepositoryInterface;
use App\Data\CommentRepository;

use App\Core\Services\ServiceInterface\TodoServiceInterface;
use App\Core\Services\TodoService;
use App\Core\RepositoryInterface\TodoRepositoryInterface;
use App\Data\TodoRepository;

use App\Core\Services\ServiceInterface\UserServiceInterface;
use App\Core\Services\UserService;
use App\Core\RepositoryInterface\UserRepositoryInterface;
use App\Data\UserRepository;

use DI\ContainerBuilder;
use \PDO;
use Psr\Container\ContainerInterface;

use function DI\get;

return function () : ContainerInterface{

    $containerBuilder = new ContainerBuilder();
    $containerBuilder->addDefinitions([

        PDO::class => function () : PDO
        {
            $host = $_ENV['DB_HOST'];
            $port = $_ENV['DB_PORT'];
            $name = $_ENV['DB_NAME'];
            $user = $_ENV['DB_USER'];
            $pass = $_ENV['DB_PWD'];

            $dsn = "mysql:host=$host;port=$port;dbname=$name;charset=utf8;";
            return new PDO($dsn, $user, $pass, [ PDO::ATTR_PERSISTENT => false ]);
        },

        // Twig::class => function () : Twig {
        //     return Twig::create(__DIR__  . '/../WebApp/views/', ['cache' => false]);
        // },


        CommentRepositoryInterface::class => get(CommentRepository::class),
        CommentServiceInterface::class => get(CommentService::class),
        TodoRepositoryInterface::class => get(TodoRepository::class),
        TodoServiceInterface::class => get(TodoService::class),
        UserRepositoryInterface::class => get(UserRepository::class),
        UserServiceInterface::class => get(UserService::class),

    ]);
    return $containerBuilder->build();
};

