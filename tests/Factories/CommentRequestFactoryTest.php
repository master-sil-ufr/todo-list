<?php

namespace Tests\Factories;

use App\WebApp\RequestFactory\CommentRequestFactory;
use PHPUnit\Framework\TestCase;
use Http\Discovery\Psr17FactoryDiscovery;

class CommentRequestFactoryTest extends  TestCase {
    public function testCommentFactoryRequestFromServerRequestWithData() : void
    {
        $request = CommentRequestFactory::fromServerRequest(
            Psr17FactoryDiscovery::findServerRequestFactory()->createServerRequest('POST', 'random')
                ->withParsedBody([
                    'taskId' => 1,
                    'createdBy' => 2,
                    'createdAt' => '',
                    'comment' => 'data',
                    'todoId' => 2
                ])
        );

        $this->assertEquals(1, $request->getTask_id());
        $this->assertEquals(2, $request->getCreated_by());
        $this->assertEquals('', $request->getCreated_at());
        $this->assertEquals('data', $request->getComment());
        $this->assertEquals(2, $request->getTodoId());
    }

    public function testCommentFactoryRequestFromServerRequestWithoutData() : void
    {
        $request = CommentRequestFactory::fromServerRequest(Psr17FactoryDiscovery::findServerRequestFactory()->createServerRequest('POST', 'random')
            ->withParsedBody([]));

        $this->assertEquals(0, $request->getTask_id());
        $this->assertEquals(0, $request->getCreated_by());
        $this->assertEquals('', $request->getCreated_at());
        $this->assertEquals('', $request->getComment());
        $this->assertEquals(0, $request->getTodoId());
    }

    public function testCommentFactoryRequestFromArgsWithData() : void
    {
        $request = CommentRequestFactory::fromServerArgs([
            'task_id' => 1,
            'created_by' => 2,
            'created_at' => '',
            'comment' => 'data',
            'todo' => 2
        ]);


        $this->assertEquals(1, $request->getTask_id());
        $this->assertEquals(2, $request->getCreated_by());
        $this->assertEquals('', $request->getCreated_at());
        $this->assertEquals('data', $request->getComment());
        $this->assertEquals(2, $request->getTodoId());
    }

    public function testCommentFactoryRequestFromArgsWithoutData() : void
    {
        $request = CommentRequestFactory::fromServerArgs([]);

        $this->assertEquals(0, $request->getTask_id());
        $this->assertEquals(0, $request->getCreated_by());
        $this->assertEquals('', $request->getCreated_at());
        $this->assertEquals('', $request->getComment());
        $this->assertEquals(0, $request->getTodoId());
    }
}