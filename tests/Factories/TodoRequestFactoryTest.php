<?php

namespace Tests\Factories;

use App\WebApp\RequestFactory\TodoRequestFactory;
use Http\Discovery\Psr17FactoryDiscovery;
use PHPUnit\Framework\TestCase;

class TodoRequestFactoryTest extends TestCase
{
    public function testTodoFactoryRequestFromServerRequestWithData() : void
    {
        $request = TodoRequestFactory::fromServerRequest(
            Psr17FactoryDiscovery::findServerRequestFactory()->createServerRequest('POST', 'random')
                ->withParsedBody([
                    'id' => 1,
                    'created_by' => 2,
                    'assignedTo' => '',
                    'title' => 'title',
                    'description' => 'description',
                    'dueDate' => '02/02/2022'
                ])
        );

        $this->assertEquals(1, $request->getId());
        $this->assertEquals(2, $request->getCreatedBy());
        $this->assertEquals('', $request->getAssignedTo());
        $this->assertEquals('title', $request->getTitle());
        $this->assertEquals('description', $request->getDescription());
        $this->assertEquals('02/02/2022', $request->getDueDate());
        $this->assertEquals(null, $request->getKeyword());
    }

    public function testTodoFactoryRequestFromServerRequestWithoutData() : void
    {
        $request = TodoRequestFactory::fromServerRequest(Psr17FactoryDiscovery::findServerRequestFactory()->createServerRequest('POST', 'random')
            ->withParsedBody([]));

        $this->assertEquals(0, $request->getId());
        $this->assertEquals(0, $request->getCreatedBy());
        $this->assertEquals('', $request->getAssignedTo());
        $this->assertEquals('', $request->getTitle());
        $this->assertEquals('', $request->getDescription());
        $this->assertEquals(null, $request->getDueDate());
        $this->assertEquals(null, $request->getKeyword());
    }

    public function testTodoFactoryRequestFromArgsWithData() : void
    {
        $request = TodoRequestFactory::fromServerArgs([
            'todo' => 1,
            'created_by' => 2,
            'assignedTo' => '',
            'title' => 'title',
            'description' => 'description',
            'dueDate' => '02/02/2022'
        ]);


        $this->assertEquals(1, $request->getId());
        $this->assertEquals(2, $request->getCreatedBy());
        $this->assertEquals('', $request->getAssignedTo());
        $this->assertEquals('title', $request->getTitle());
        $this->assertEquals('description', $request->getDescription());
        $this->assertEquals('02/02/2022', $request->getDueDate());
        $this->assertEquals(null, $request->getKeyword());
    }

    public function testTodoFactoryRequestFromArgsWithoutData() : void
    {
        $request = TodoRequestFactory::fromServerArgs([]);

        $this->assertEquals(0, $request->getId());
        $this->assertEquals(0, $request->getCreatedBy());
        $this->assertEquals('', $request->getAssignedTo());
        $this->assertEquals('', $request->getTitle());
        $this->assertEquals('', $request->getDescription());
        $this->assertEquals(null, $request->getDueDate());
        $this->assertEquals(null, $request->getKeyword());
    }
}