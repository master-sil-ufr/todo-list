<?php

namespace Tests\Factories;

use App\WebApp\RequestFactory\UserRequestFactory;
use Http\Discovery\Psr17FactoryDiscovery;
use PHPUnit\Framework\TestCase;

class UserRequestFactoryTest extends TestCase
{
    public function testUserFactoryRequestFromServerRequestWithData() : void
    {
        $request = UserRequestFactory::fromServerRequest(
            Psr17FactoryDiscovery::findServerRequestFactory()->createServerRequest('POST', 'random')
                ->withParsedBody([
                    'email' => 'email@email.fr',
                    'password' => 'pwd',
                    'Confirmpassword' => 'pwd'
                ])
        );

        $this->assertEquals('email@email.fr', $request->getEmail());
        $this->assertEquals('pwd', $request->getPwd());
        $this->assertEquals('pwd', $request->getConfirmPWD());
        $this->assertEquals(true, $request->getPwd() === $request->getConfirmPWD());
    }

    public function testUserFactoryRequestFromServerRequestWithoutData() : void
    {
        $request = UserRequestFactory::fromServerRequest(Psr17FactoryDiscovery::findServerRequestFactory()->createServerRequest('POST', 'random')
            ->withParsedBody([]));

        $this->assertEquals('', $request->getEmail());
        $this->assertEquals('', $request->getPwd());
        $this->assertEquals('', $request->getConfirmPWD());
        $this->assertEquals(true, $request->getPwd() === $request->getConfirmPWD());
    }

    public function testUserFactoryRequestFromServerRequestWithDifferentPwd() : void
    {
        $request = UserRequestFactory::fromServerRequest(
            Psr17FactoryDiscovery::findServerRequestFactory()->createServerRequest('POST', 'random')
                ->withParsedBody([
                    'email' => 'email@email.fr',
                    'password' => 'pwd',
                    'Confirmpassword' => 'other'
                ])
        );

        $this->assertEquals('email@email.fr', $request->getEmail());
        $this->assertEquals('pwd', $request->getPwd());
        $this->assertEquals('other', $request->getConfirmPWD());
        $this->assertEquals(false, $request->getPwd() === $request->getConfirmPWD());
    }
}