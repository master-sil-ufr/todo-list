<?php

namespace Tests\Services;

use App\Core\Entities\Todo;
use App\Core\Exceptions\TodoExceptions\BadTodoIdException;
use App\Core\Exceptions\TodoExceptions\UnknownTodoIdException;
use App\Core\Requests\TodoRequest;
use App\Core\Services\TodoService;
use App\Data\TodoRepository;

class TodoServiceTest extends \PHPUnit\Framework\TestCase
{
    private TodoRepository $todoRepository;

    public function setUp(): void
    {
        $this->todoRepository = $this->createMock(TodoRepository::class);
    }

    public function testShouldInsertNewTodo()
    {
        $this->todoRepository->expects($this->once())->method('insertTodo');


        $request = new TodoRequest(1, 1, null, 'title', 'desc', '', '');
        $service = new TodoService($this->todoRepository);
        $service->insertTodo($request);
    }

    public function testShouldGetTodosFromEmail()
    {
        $this->todoRepository->expects($this->once())->method('getTodos')
            ->with('email@email.fr')
            ->willReturn([
                new Todo(2, 1, null, 'title', 'desc', '22/12/2022', null)]);

        $service = new TodoService($this->todoRepository);
        $service->getTodos('email@email.fr', '');
    }

    public function testShouldGetTodosFromKeyword()
    {
        $this->todoRepository->expects($this->once())->method('getTodos')
            ->willReturn([
                new Todo(2, 1, null, 'title', 'desc', '22/12/2022', null)]);

        $service = new TodoService($this->todoRepository);
        $service->getTodos('', 'title');
    }

    public function testShoudThrowExceptionOnUnknownTodoId()
    {
        $this->todoRepository->expects($this->once())->method('getTodoById')
            ->with(1)
            ->willThrowException(new UnknownTodoIdException());

        $this->expectException(UnknownTodoIdException::class);

        $service = new TodoService($this->todoRepository);
        $service->getTodoById(1);
    }

    public function testShoudThrowExceptionOnBadTodoId()
    {
        $this->todoRepository->expects($this->never())->method('getTodoById');

        $this->expectException(BadTodoIdException::class);
        $service = new TodoService($this->todoRepository);
        $service->getTodoById(0);
    }

    public function testShouldReturnTodoInstance()
    {
        $this->todoRepository->expects($this->once())->method('getTodoById')
            ->with(1)
            ->willReturn(
                new Todo(1, 1, null, 'title', 'desc', '22/12/2022', null));

        $service = new TodoService($this->todoRepository);
        $service->getTodoById(1);
    }

    public function testShouldDeleteTodoInstanceFromId()
    {
        $this->todoRepository->expects($this->once())->method('deleteTodo')
            ->with(1);

        $service = new TodoService($this->todoRepository);
        $service->deleteTodo(1);
    }
}