<?php

namespace Tests\Services;

use App\Core\Entities\User;
use App\Core\Exceptions\UserExceptions\NullUserException;
use App\Core\Exceptions\UserExceptions\UnknownUserException;
use App\Core\Requests\UserRequest;
use App\Core\Services\UserService;
use App\Data\UserRepository;
use PHPUnit\Framework\TestCase;

class UserServiceTest extends TestCase
{
    private UserRepository $userRepository;

    public function setUp(): void
    {
        $this->userRepository = $this->createMock(UserRepository::class);
    }

    public function testShouldReturnOneUser() : void
    {
        $this->userRepository->expects($this->once())->method('checkConnexion')
            ->willReturn(new User(1, 'email@email.fr', 'pwd'));

        $request = new UserRequest('email@email.fr', 'pwd');
        $service = new UserService($this->userRepository);
        $this->assertEquals($service->checkConnexion($request), new User(1, 'email@email.fr', 'pwd'));

    }

    public function testShouldThrowExceptionOnEmptyUser()
    {
        $this->userRepository->expects($this->once())->method('checkConnexion')
            ->willThrowException(new UnknownUserException());


        $request = new UserRequest('', 'pwd');
        $service = new UserService($this->userRepository);
        $this->assertSame($service->checkConnexion($request), false);
    }

    public function testShouldCreateNewUserInstance()
    {
        $this->userRepository->expects($this->once())->method('createUser')
            ->willReturn(new User(2, 'email@email.fr', 'pwd'));

        $request = new UserRequest('email@email.fr', 'pwd');
        $service = new UserService($this->userRepository);
        $service->createUser($request);
    }

    public function testShouldGetUserInstances()
    {
        $this->userRepository->expects($this->once())->method('getUsers')
            ->willReturn([
                new User(2, 'email1@email.fr', 'pwd')]);

        $service = new UserService($this->userRepository);
        $this->assertEquals($service->getUsers(), [new User(2, 'email1@email.fr', 'pwd')]);
    }

    public function testShouldReturnUserInstanceFromId()
    {
        $this->userRepository->expects($this->once())->method('getUserById')
            ->with(2)
            ->willReturn(new User(2, 'email@email.fr', 'pwd'));

        $service = new UserService($this->userRepository);

        $this->assertEquals($service->getUserById(2), new User(2, 'email@email.fr', 'pwd'));
    }

    public function testShouldThrowExceptionOnUserInstanceFromId()
    {
        $this->userRepository->expects($this->once())->method('getUserById')
            ->willThrowException(new UnknownUserException());

        $this->expectException(UnknownUserException::class);

        $service = new UserService($this->userRepository);
        $service->getUserById(2);
    }

    public function testShoudReturnUserInstanceFromEmail()
    {
        $this->userRepository->expects($this->once())->method('getUserByEmail')
            ->willReturn(new User(2, 'email@email.fr', 'pwd'));

        $service = new UserService($this->userRepository);
        $this->assertEquals($service->getUserByEmail('email@email.fr'), new User(2, 'email@email.fr', 'pwd'));
    }

    public function testShoudThrowExceptionOnUserInstanceFromEmail()
    {
        $this->userRepository->expects($this->once())->method('getUserByEmail')
            ->willThrowException(new UnknownUserException());

        $this->expectException(UnknownUserException::class);
        $service = new UserService($this->userRepository);
        $service->getUserByEmail('email@email.fr');
    }

    public function testShoudThrowExceptionOnNullUserInstanceFromEmail()
    {
        $this->userRepository->expects($this->never())->method('getUserByEmail');

        $this->expectException(NullUserException::class);
        $service = new UserService($this->userRepository);
        $service->getUserByEmail('null');
    }
}