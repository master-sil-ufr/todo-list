<?php

namespace Tests\Services;
use App\Core\Entities\Comment;
use App\Core\Exceptions\CommentExceptions\EmptyCommentException;
use App\Core\Requests\CommentRequest;
use App\Core\Services\CommentService;
use App\Data\CommentRepository;
use \PHPUnit\Framework\TestCase;

class CommentServiceTest extends TestCase
{
    private CommentRepository $commentRepository;

    public function setUp(): void
    {
        $this->commentRepository = $this->createMock(CommentRepository::class);
    }

    public function testShouldNotInsertCommentWhenCommentIsEmpty(): void
    {
        $this->commentRepository->expects($this->never())
            ->method('addComment');

        $this->expectException(EmptyCommentException::class);

        $request = new CommentRequest(1, 1, '', '', 2);

        $service = new CommentService($this->commentRepository);
        $service->addComment($request->getComment(), $request->getCreated_by(), $request->getTask_id());
    }

    /**
     * @throws EmptyCommentException
     */
    public function testShouldAddCommentWithNonEmptyComment()
    {
        $this->commentRepository->expects($this->once())
            ->method('addComment');

        $request = new CommentRequest(1, 1, '', 'comment', 2);

        $service = new CommentService($this->commentRepository);
        $service->addComment($request->getComment(), $request->getCreated_by(), $request->getTask_id());
    }

    public function testShouldReturnCommentsFromTaskId()
    {
        $this->commentRepository->expects($this->once())
            ->method('getComments')->willReturn([new Comment(1, 1, 1, '25/12/2022', 'toto')]);

        $service = new CommentService($this->commentRepository);
        $service->getComments(1);
    }
}