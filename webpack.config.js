// Run : npx webpack --config webpack.config.js (in docker)

const path = require('path')

module.exports = {
    entry: ['./src/WebApp/views/assets/js/main.js'],
    output: {
        path: path.resolve('./public/js/'),
        filename: 'index.js'
    },
    mode: 'development'
}