FROM php:7.4-apache

# Update system
RUN apt-get update && rm /etc/apt/preferences.d/no-debian-php && apt-get install --yes git libzip-dev zip

# Install nodejs & npm
RUN curl -sL https://deb.nodesource.com/setup_15.x | bash - \
   && apt-get install -y nodejs

# Install compass
RUN apt-get install -y ruby-full \
    && gem install compass

# Install webpack
RUN npm install --global webpack webpack-cli

# Install composer
ENV COMPOSER_HOME "/home/www-data/.composer"
RUN mkdir -p $COMPOSER_HOME && chmod -R 777 $COMPOSER_HOME && curl -sS https://getcomposer.org/installer | php \
  && chmod +x composer.phar && mv composer.phar /usr/local/bin/composer

RUN mkdir /srv/app
COPY vhost.conf /etc/apache2/sites-available/000-default.conf

RUN pecl install xdebug-2.9.2 && \
 docker-php-ext-enable xdebug && \
 docker-php-ext-install pdo pdo_mysql bcmath && \
 chown -R www-data:www-data /srv/app && \
 a2enmod rewrite
