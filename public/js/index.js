/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/WebApp/views/assets/js/ajax.js":
/*!********************************************!*\
  !*** ./src/WebApp/views/assets/js/ajax.js ***!
  \********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\nconst Ajax = {\n    filter : async function (data, callback) {  \n        await $.post(\"/dashboard\", data, callback)\n    },\n\n    getTodo : async function (link, callback) {\n        await $.get(link, callback)\n    },\n\n    getComments : async function (link, callback) {\n        await $.get(link, callback)\n    },\n    \n    getUsers :async function (callback) {\n        await $.get(\"/users\", callback)\n    },\n\n    addComment: async function (data, callback) {\n        await $.post(\"/dashboard/add/comment\", data, callback)\n    }\n};\n\n\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Ajax);\n\n//# sourceURL=webpack://tortue-todo/./src/WebApp/views/assets/js/ajax.js?");

/***/ }),

/***/ "./src/WebApp/views/assets/js/functions.js":
/*!*************************************************!*\
  !*** ./src/WebApp/views/assets/js/functions.js ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\nconst Functions = {\n\n    setusers : function (users) {\n    users = $.parseJSON(users)\n    console.log(users, users.length, typeof users)\n    let options = '';\n    for (let i = 0; i < users.length; i++) {\n        options += '<option value=\"' + i + '\">' + users[i]['email'] + '</option>\\n'\n    }\n    $('.task-kind').empty();\n    $('.task-kind').append('<select id=\"userTask\" class=\"form-select\">\\\n            ' + options +    '\\\n            </select>');\n    },\n\n    setfilter : function (filter) {\n    console.log(filter)\n    filter = $.parseJSON(filter)\n    $('.liste').empty()\n    filter.forEach(f => {\n        $('.liste').append(\"<a class='todoLink' id='todoLink\"+f['id'] +\"' href=\\\"javascript:void(0);\\\">\" + f['title'] + ' </a>')\n    });\n    },\n\n    settodocontainer : function (todo) {\n        todo = $.parseJSON(todo)\n        todo = todo[0]\n    $(\".MainContent .TodoContent h2\").text(todo['title'])\n    if(todo['assigned_to']==null && todo['due_date']!=null)\n        $(\".MainContent .TodoContent .assigned\").text(\"A faire avant le \"+todo['due_date'])\n    else if(todo['assigned_to']!=null && todo['due_date']==null)\n        $(\".MainContent .TodoContent .assigned\").text(\"A faire par \"+todo['assigned_to_email'])\n    else if(todo['assigned_to']!=null && todo['due_date']!=null)\n        $(\".MainContent .TodoContent .assigned\").text(\"A faire par \"+todo['assigned_to_email']+\" avant le \"+todo['due_date'])\n    else\n        $(\".MainContent .TodoContent .assigned\").text(\"\")\n    $(\".MainContent .TodoContent textarea\").text(todo['description'])\n    $(\".MainContent .TodoContent .creator\").text(\"Créé par \" + todo['created_by_email'] + \" le \" + todo['created_at'])\n    $(\".MainContent .TodoContent #deleteTodo\").attr(\"action\", \"/dashboard/delete/todo/\"+todo['id'])\n    $(\"#taskId\").val(todo['id'])\n\n    },\n\n    setcommentcontainer : function (comment) {\n    console.log(comment)\n    comment = $.parseJSON(comment)\n    $('.displayComments').empty()\n    console.log(comment.length)\n    if (comment.length != 0) {\n\n        let list = ''\n        for (let i = comment.length-1; i >= 0; i--) {\n            // Ajouter la date du commentaire\n            list += '<li> [' + comment[i]['created_at'] + ' ] ' + comment[i]['comment'] + '</li>'\n        }\n        \n        $('.displayComments').append('<ul>' + list + '</ul>')\n    }\n\n    },\n}\n\n\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Functions);\n\n//# sourceURL=webpack://tortue-todo/./src/WebApp/views/assets/js/functions.js?");

/***/ }),

/***/ "./src/WebApp/views/assets/js/main.js":
/*!********************************************!*\
  !*** ./src/WebApp/views/assets/js/main.js ***!
  \********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _ajax__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ajax */ \"./src/WebApp/views/assets/js/ajax.js\");\n/* harmony import */ var _functions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./functions */ \"./src/WebApp/views/assets/js/functions.js\");\n\n\n\n$('#todo').click(function(event) {\n    event.preventDefault();\n\n    $.get(\"/dashboard/ajax\", function (response) {  \n        $(\"#res\").load(\"/dashboard/ajax\")\n    })\n})\n\n\n$('.form-select').change( async function () { \n    console.log($(this).val())\n    if ($(this).val() == 1) {\n        _ajax__WEBPACK_IMPORTED_MODULE_0__[\"default\"].getUsers(setusers)\n    }else if ($(this).val() == 2) {\n        $('.task-kind').empty();\n        $('.task-kind').append('<input id=\"Search\" type=\"text\" name=\"Search\" placeholder=\"Mot clé\" class=\"form-control\"/>');\n    }\n});\n\n$('#filter').submit(function (event) {\n    let form = $('#filter').serializeArray()\n    event.preventDefault()\n    _ajax__WEBPACK_IMPORTED_MODULE_0__[\"default\"].filter(form, _functions__WEBPACK_IMPORTED_MODULE_1__[\"default\"].setfilter)\n})\n\n$('.liste').on('click', '.todoLink', function (e) {\n    e.preventDefault()\n    let todoLinkId = $(this).attr('id');\n    let todoId = todoLinkId.substring(\"todoLink\".length,todoLinkId.length)\n    // let todoId = todoLinkId.substr(\"todoLink\".length,1)\n    let todolink = \"/dashboard/todo/\" + todoId\n    let commentlink = \"/dashboard/comment/\" + todoId\n\n    console.log('todoLink' + todolink);\n    console.log('todoLinkId' + todoLinkId);\n    console.log('todoId' + todoId);\n\n    _ajax__WEBPACK_IMPORTED_MODULE_0__[\"default\"].getTodo(todolink, _functions__WEBPACK_IMPORTED_MODULE_1__[\"default\"].settodocontainer)\n    _ajax__WEBPACK_IMPORTED_MODULE_0__[\"default\"].getComments(commentlink, _functions__WEBPACK_IMPORTED_MODULE_1__[\"default\"].setcommentcontainer)\n})\n\n$('.AddComment').submit(function (e) {\n    let form = $('.AddComment').serializeArray()\n    e.preventDefault()\n    \n    $('.AddComment textarea').val(\"\");\n\n    _ajax__WEBPACK_IMPORTED_MODULE_0__[\"default\"].addComment(form, _functions__WEBPACK_IMPORTED_MODULE_1__[\"default\"].setcommentcontainer)\n})\n\n$('.flash').show()\nconsole.log('hello ' + $('.flash'))\nsetTimeout(() => $('.flash').hide())\n\n//# sourceURL=webpack://tortue-todo/./src/WebApp/views/assets/js/main.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = __webpack_require__("./src/WebApp/views/assets/js/main.js");
/******/ 	
/******/ })()
;