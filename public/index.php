<?php 

use Slim\Factory\AppFactory;
use Slim\Views\TwigMiddleware;
use Slim\Views\Twig;

use App\WebApp\Controllers\PagesController;
use App\WebApp\Controllers\FormController;
use App\WebApp\Controllers\AjaxController;

session_start();

require __DIR__ . "/../vendor/autoload.php";

require __DIR__ . '/../src/Infrastructure/env.php';

$container = require __DIR__ . '/../src/Infrastructure/dependencies.php';

AppFactory::setContainer($container());

$app = AppFactory::create();

// var_dump( $container());


$twig = Twig::create(__DIR__ . '/../src/WebApp/views/', ['cache' => false]);
$app->add(TwigMiddleware::create($app, $twig));

// $user = new User();
// $todos = new Todos();

require __DIR__ . './../src/Infrastructure/routes.php';


$app->run();