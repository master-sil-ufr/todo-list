# Projet TODO

the project can be launched :
```bash
docker-compose up 
```
## Membres

- BURCKEL Luc
- BENZAHIA Mohamed
- MUKAYA-GAKALI Caleb


## Tasks 

- [X] Langage de programmation: **(PHP 8 / XHTML / CSS / JS)**
- [X] Utilisation de composer pour les dépendances PHP
- [X] Utilisation du Framework **(Slim 4)**
- [X] Accès à la base de données MySQL avec **(PDO)**
- [X] Utilisation du framework d’injection de dépendances **(PHP-DI)**
- [X] Utilisation de variables d’environnement pour la configuration de l’app
- [X] Utilisation de NPM pour les dépendances **(node)**
- [X] Utilisation du framework **(Bootstrap 5)**
- [X] Site web responsive design utilisable sur 3 types de device: smartphone, tablette et desktop
- [X] Utilisation du moteur de template **(Twig)**
- [X] Utilisation du pré-processeur de feuille de style Sass
- [X] Utilisation du framework javascript **(jQuery)**
- [X] Mise en place d’une structure **(MVC (modèle - vue - contrôleur))**
- [X] Utilisation de **(docker)** et de docker compose pour l’infrastructure
