<?php return array(
    'root' => array(
        'name' => 'tortue/todo',
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'reference' => '8f028f2e98355a83f1e024d3e1adfb52f12a8d10',
        'type' => 'project',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'doctrine/instantiator' => array(
            'pretty_version' => '1.4.1',
            'version' => '1.4.1.0',
            'reference' => '10dcfce151b967d20fde1b34ae6640712c3891bc',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/instantiator',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'fig/http-message-util' => array(
            'pretty_version' => '1.1.5',
            'version' => '1.1.5.0',
            'reference' => '9d94dc0154230ac39e5bf89398b324a86f63f765',
            'type' => 'library',
            'install_path' => __DIR__ . '/../fig/http-message-util',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'graham-campbell/result-type' => array(
            'pretty_version' => 'v1.1.0',
            'version' => '1.1.0.0',
            'reference' => 'a878d45c1914464426dc94da61c9e1d36ae262a8',
            'type' => 'library',
            'install_path' => __DIR__ . '/../graham-campbell/result-type',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'laravel/serializable-closure' => array(
            'pretty_version' => 'v1.2.2',
            'version' => '1.2.2.0',
            'reference' => '47afb7fae28ed29057fdca37e16a84f90cc62fae',
            'type' => 'library',
            'install_path' => __DIR__ . '/../laravel/serializable-closure',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'myclabs/deep-copy' => array(
            'pretty_version' => '1.11.0',
            'version' => '1.11.0.0',
            'reference' => '14daed4296fae74d9e3201d2c4925d1acb7aa614',
            'type' => 'library',
            'install_path' => __DIR__ . '/../myclabs/deep-copy',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'nikic/fast-route' => array(
            'pretty_version' => 'v1.3.0',
            'version' => '1.3.0.0',
            'reference' => '181d480e08d9476e61381e04a71b34dc0432e812',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nikic/fast-route',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'nikic/php-parser' => array(
            'pretty_version' => 'v4.15.2',
            'version' => '4.15.2.0',
            'reference' => 'f59bbe44bf7d96f24f3e2b4ddc21cd52c1d2adbc',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nikic/php-parser',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'phar-io/manifest' => array(
            'pretty_version' => '2.0.3',
            'version' => '2.0.3.0',
            'reference' => '97803eca37d319dfa7826cc2437fc020857acb53',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phar-io/manifest',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'phar-io/version' => array(
            'pretty_version' => '3.2.1',
            'version' => '3.2.1.0',
            'reference' => '4f7fd7836c6f332bb2933569e566a0d6c4cbed74',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phar-io/version',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'php-di/invoker' => array(
            'pretty_version' => '2.3.3',
            'version' => '2.3.3.0',
            'reference' => 'cd6d9f267d1a3474bdddf1be1da079f01b942786',
            'type' => 'library',
            'install_path' => __DIR__ . '/../php-di/invoker',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'php-di/php-di' => array(
            'pretty_version' => '6.4.0',
            'version' => '6.4.0.0',
            'reference' => 'ae0f1b3b03d8b29dff81747063cbfd6276246cc4',
            'type' => 'library',
            'install_path' => __DIR__ . '/../php-di/php-di',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'php-di/phpdoc-reader' => array(
            'pretty_version' => '2.2.1',
            'version' => '2.2.1.0',
            'reference' => '66daff34cbd2627740ffec9469ffbac9f8c8185c',
            'type' => 'library',
            'install_path' => __DIR__ . '/../php-di/phpdoc-reader',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'php-http/discovery' => array(
            'pretty_version' => '1.14.3',
            'version' => '1.14.3.0',
            'reference' => '31d8ee46d0215108df16a8527c7438e96a4d7735',
            'type' => 'library',
            'install_path' => __DIR__ . '/../php-http/discovery',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'phpoption/phpoption' => array(
            'pretty_version' => '1.9.0',
            'version' => '1.9.0.0',
            'reference' => 'dc5ff11e274a90cc1c743f66c9ad700ce50db9ab',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpoption/phpoption',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'phpunit/php-code-coverage' => array(
            'pretty_version' => '9.2.22',
            'version' => '9.2.22.0',
            'reference' => 'e4bf60d2220b4baaa0572986b5d69870226b06df',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpunit/php-code-coverage',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'phpunit/php-file-iterator' => array(
            'pretty_version' => '3.0.6',
            'version' => '3.0.6.0',
            'reference' => 'cf1c2e7c203ac650e352f4cc675a7021e7d1b3cf',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpunit/php-file-iterator',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'phpunit/php-invoker' => array(
            'pretty_version' => '3.1.1',
            'version' => '3.1.1.0',
            'reference' => '5a10147d0aaf65b58940a0b72f71c9ac0423cc67',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpunit/php-invoker',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'phpunit/php-text-template' => array(
            'pretty_version' => '2.0.4',
            'version' => '2.0.4.0',
            'reference' => '5da5f67fc95621df9ff4c4e5a84d6a8a2acf7c28',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpunit/php-text-template',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'phpunit/php-timer' => array(
            'pretty_version' => '5.0.3',
            'version' => '5.0.3.0',
            'reference' => '5a63ce20ed1b5bf577850e2c4e87f4aa902afbd2',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpunit/php-timer',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'phpunit/phpunit' => array(
            'pretty_version' => '9.5.27',
            'version' => '9.5.27.0',
            'reference' => 'a2bc7ffdca99f92d959b3f2270529334030bba38',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpunit/phpunit',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'psr/container' => array(
            'pretty_version' => '1.1.2',
            'version' => '1.1.2.0',
            'reference' => '513e0666f7216c7459170d56df27dfcefe1689ea',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/container',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/container-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '^1.0',
            ),
        ),
        'psr/http-factory' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'reference' => '12ac7fcd07e5b077433f5f2bee95b3a771bf61be',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-factory',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/http-factory-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-message' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-message',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/http-message-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-server-handler' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'reference' => 'aff2f80e33b7f026ec96bb42f63242dc50ffcae7',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-server-handler',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/http-server-middleware' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'reference' => '2296f45510945530b9dceb8bcedb5cb84d40c5f5',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-server-middleware',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/log' => array(
            'pretty_version' => '1.1.4',
            'version' => '1.1.4.0',
            'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/log',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'ralouphie/getallheaders' => array(
            'pretty_version' => '3.0.3',
            'version' => '3.0.3.0',
            'reference' => '120b605dfeb996808c31b6477290a714d356e822',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ralouphie/getallheaders',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'sebastian/cli-parser' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'reference' => '442e7c7e687e42adc03470c7b668bc4b2402c0b2',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/cli-parser',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'sebastian/code-unit' => array(
            'pretty_version' => '1.0.8',
            'version' => '1.0.8.0',
            'reference' => '1fc9f64c0927627ef78ba436c9b17d967e68e120',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/code-unit',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'sebastian/code-unit-reverse-lookup' => array(
            'pretty_version' => '2.0.3',
            'version' => '2.0.3.0',
            'reference' => 'ac91f01ccec49fb77bdc6fd1e548bc70f7faa3e5',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/code-unit-reverse-lookup',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'sebastian/comparator' => array(
            'pretty_version' => '4.0.8',
            'version' => '4.0.8.0',
            'reference' => 'fa0f136dd2334583309d32b62544682ee972b51a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/comparator',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'sebastian/complexity' => array(
            'pretty_version' => '2.0.2',
            'version' => '2.0.2.0',
            'reference' => '739b35e53379900cc9ac327b2147867b8b6efd88',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/complexity',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'sebastian/diff' => array(
            'pretty_version' => '4.0.4',
            'version' => '4.0.4.0',
            'reference' => '3461e3fccc7cfdfc2720be910d3bd73c69be590d',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/diff',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'sebastian/environment' => array(
            'pretty_version' => '5.1.4',
            'version' => '5.1.4.0',
            'reference' => '1b5dff7bb151a4db11d49d90e5408e4e938270f7',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/environment',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'sebastian/exporter' => array(
            'pretty_version' => '4.0.5',
            'version' => '4.0.5.0',
            'reference' => 'ac230ed27f0f98f597c8a2b6eb7ac563af5e5b9d',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/exporter',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'sebastian/global-state' => array(
            'pretty_version' => '5.0.5',
            'version' => '5.0.5.0',
            'reference' => '0ca8db5a5fc9c8646244e629625ac486fa286bf2',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/global-state',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'sebastian/lines-of-code' => array(
            'pretty_version' => '1.0.3',
            'version' => '1.0.3.0',
            'reference' => 'c1c2e997aa3146983ed888ad08b15470a2e22ecc',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/lines-of-code',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'sebastian/object-enumerator' => array(
            'pretty_version' => '4.0.4',
            'version' => '4.0.4.0',
            'reference' => '5c9eeac41b290a3712d88851518825ad78f45c71',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/object-enumerator',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'sebastian/object-reflector' => array(
            'pretty_version' => '2.0.4',
            'version' => '2.0.4.0',
            'reference' => 'b4f479ebdbf63ac605d183ece17d8d7fe49c15c7',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/object-reflector',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'sebastian/recursion-context' => array(
            'pretty_version' => '4.0.4',
            'version' => '4.0.4.0',
            'reference' => 'cd9d8cf3c5804de4341c283ed787f099f5506172',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/recursion-context',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'sebastian/resource-operations' => array(
            'pretty_version' => '3.0.3',
            'version' => '3.0.3.0',
            'reference' => '0f4443cb3a1d92ce809899753bc0d5d5a8dd19a8',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/resource-operations',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'sebastian/type' => array(
            'pretty_version' => '3.2.0',
            'version' => '3.2.0.0',
            'reference' => 'fb3fe09c5f0bae6bc27ef3ce933a1e0ed9464b6e',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/type',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'sebastian/version' => array(
            'pretty_version' => '3.0.2',
            'version' => '3.0.2.0',
            'reference' => 'c6c1022351a901512170118436c764e473f6de8c',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/version',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'slim/psr7' => array(
            'pretty_version' => '1.6',
            'version' => '1.6.0.0',
            'reference' => '3471c22c1a0d26c51c78f6aeb06489d38cf46a4d',
            'type' => 'library',
            'install_path' => __DIR__ . '/../slim/psr7',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'slim/slim' => array(
            'pretty_version' => '4.11.0',
            'version' => '4.11.0.0',
            'reference' => 'b0f4ca393ea037be9ac7292ba7d0a34d18bac0c7',
            'type' => 'library',
            'install_path' => __DIR__ . '/../slim/slim',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'slim/twig-view' => array(
            'pretty_version' => '3.3.0',
            'version' => '3.3.0.0',
            'reference' => 'df6dd6af6bbe28041be49c9fb8470c2e9b70cd98',
            'type' => 'library',
            'install_path' => __DIR__ . '/../slim/twig-view',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-ctype' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '5bbc823adecdae860bb64756d639ecfec17b050a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-ctype',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '8ad114f6b39e2c98a8b0e3bd907732c207c2b534',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php80' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '7a6ff3f1959bb01aefccb463a0f2cd3d3d2fd936',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php80',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php81' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '707403074c8ea6e2edaf8794b0157a0bfa52157a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php81',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'theseer/tokenizer' => array(
            'pretty_version' => '1.2.1',
            'version' => '1.2.1.0',
            'reference' => '34a41e998c2183e22995f158c581e7b5e755ab9e',
            'type' => 'library',
            'install_path' => __DIR__ . '/../theseer/tokenizer',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'tortue/todo' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => '8f028f2e98355a83f1e024d3e1adfb52f12a8d10',
            'type' => 'project',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'twbs/bootstrap' => array(
            'pretty_version' => 'v5.2.3',
            'version' => '5.2.3.0',
            'reference' => 'cb021439c683d9805e2864c58095b92d405e9b11',
            'type' => 'library',
            'install_path' => __DIR__ . '/../twbs/bootstrap',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'twig/twig' => array(
            'pretty_version' => 'v3.4.3',
            'version' => '3.4.3.0',
            'reference' => 'c38fd6b0b7f370c198db91ffd02e23b517426b58',
            'type' => 'library',
            'install_path' => __DIR__ . '/../twig/twig',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'twitter/bootstrap' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.2.3',
            ),
        ),
        'vlucas/phpdotenv' => array(
            'pretty_version' => 'v5.5.0',
            'version' => '5.5.0.0',
            'reference' => '1a7ea2afc49c3ee6d87061f5a233e3a035d0eae7',
            'type' => 'library',
            'install_path' => __DIR__ . '/../vlucas/phpdotenv',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
